## Teil 2:  Umgang mit Clubräumen und Clubinventar

Der Verein verfügt über eigene Räumlichkeiten und ist Eigentümer von
Maschinen, Werkzeugen, Messgeräten, Haushaltsgeräten, Möbeln u.s.w. Der Sinn
dieser Anschaffungen ist selbstverständlich die Nutzungsmöglichkeit für alle
Mitglieder. Um Fehlbehandlungen und Beschädigungen zu minimieren und eine
möglichst hohe Verfügbarkeit für alle sicherzustellen, bitten wir um Beachtung
der folgenden Regelungen:

### Allgemein:

- Fehler passieren, Dinge gehen kaputt. Das ist kein Weltuntergang. Bitte
  melde einen entstandenen Schaden aber umgehend an die Members-Liste, dem
  Plenum oder zumindest dem Vorstand.

- Nach der Nutzung von Räumen und Clubinventar ist beides in einem nutzbaren
  Zustand zu hinterlassen. Faustregel: Hinterlasse den Club in einem besseren
  Zustand, als du ihn vorgefunden hast.

- Innerhalb der Clubräume ist Rauchen nur in der Lounge gestattet.

- Schreckschusswaffen und Schusswaffen sind in den Clubräumen verboten

### Hauptraum:

- Achte nach 22 Uhr auf eine nachbarschaftsverträgliche Lautstärke.

- Pizzakartons bitte gleich in die Mülltonnen im Hof.

### Küche:

- Jeder will die Küche in einem nutzbaren Zustand vorfinden. Auch, wenn du für
  mehrere kochst, solltest du sicherstellen, dass auch jemand die Küche
  hinterher wieder sauber macht. Faustregel: Wer isst, der putzt. Wer kocht, ist
  verantwortlich, dass das auch passiert.

### Labor:

- Achte am Ende darauf, dass alle Elektrogeräte ausgeschaltet sind. Erst
  anschließend den Hauptschalter deaktivieren!

- Lasercutter und 3D-Drucker sind komplizierter und fragiler als du denkst.
  Daher bitte nur nach Einweisung benutzen. Siehe dazu auch die entsprechenden
  Seiten im Wiki.

- Das Labor ist kein Lagerplatz für private Kisten. Diese gehören ins
  Zwischenlager oder in die privaten Schließfächer.

### Veranstaltungen & Termine:

- Jeder Termin benötigt eine verantwortliche Person (=Organizer), die sich darum 
  kümmert, dass die Räumlichkeiten geöffnet, die Regeln eingehalten und die Räume in 
  einem ordentlichen Zustand hinterlassen werden.

- Kalendereintrag: Alle Termine werden im Kalendersystem eingetragen. Vor dem Eintragen 
  neuer Termine sollte überprüft werden, ob es bereits geplante Veranstaltungen gibt, 
  um Überschneidungen zu vermeiden. Bei Terminkonflikten ist die Abstimmung mit den 
  betroffenen Gruppen erforderlich.

- Plenumsbeschluss: Nicht-Club-bezogene Veranstaltungen benötigen die Genehmigung 
  des Plenums. Einmalige Club-bezogene Veranstaltungen können ohne Plenumsbeschluss 
  organisiert werden, sofern keine Konflikte mit anderen Terminen oder Regeln bestehen. 
  Bei regelmäßig wiederkehrenden Veranstaltungen ist ein einmaliger Plenumsbeschluss 
  ausreichend.

### Anmerkungen zu Teil 2:

- Die oben genannten Regeln wurden ohne Begründungen angegeben, um die
  Übersichtlichkeit zu gewährleisten. Falls dir der Sinn hinter einigen Regeln
  nicht klar ist, bitte lieber jemanden fragen, statt die Regeln zu missachten.
