## Teil 5: Sanktionen

Fehler dürfen **selten** und **fahrlässig** passieren. Fortgesetztes oder
vorsätzliches Fehlverhalten wird hingegen nicht toleriert. Dazu hat der Club
folgende Möglichkeiten dieses zu sanktionieren:

- nicht-öffentliche Verwarnung durch ein Mitglied des Vorstands
- öffentliche Verwarnung durch ein Mitglied des Vorstands oder durch einen
  Plenumsentscheid in einfacher Mehrheit
- Aufruf zur Wiedergutmachung (z.B. Aufräumen) durch einen Plenumsentscheid in
  einfacher Mehrheit. Die eingeforderte Wiedergutmachung muss dabei in enger
  Beziehung zum Fehlverhalten stehen
- Entzug der Schließberechtigung bei Keyholdern für eine Dauer von maximal
  einem Monat durch einen Plenumsentscheid in einfacher Mehrheit oder einen
  Entscheid der Keyholder in einfacher Mehrheit
- Clubraumverbot für einzelne Clubräume oder den gesamten Club für eine
  Dauer von maximal einem Monat durch einen Plenumsentscheid in einfacher
  Mehrheit
- Dauerhafter Entzug der Schließberechtigung bei Keyholdern durch einen
  Plenumsentscheid in 2/3-Mehrheit. Dieser Entzug ist nur durch das Plenum
  in 2/3-Mehrheit wieder aufhebbar.
- Dauerhaftes Clubraumverbot durch einen Plenumsentscheid in 2/3-Mehrheit
- Ausschluss aus dem Verein durch den Vorstand nach Empfehlung durch das
  Plenum in 2/3-Mehrheit

### Anmerkungen zu Teil 5:

- Die obige Liste ist in aufsteigender Reihenfolge der Härte sortiert
- Für ein ähnliches Fehlverhalten eines Mitglieds soll nicht zweimal die
  selbe Konsequenz oder gar eine schwächere folgen.
- Eine "Öffentliche Verwarnung" wird bei einem Plenum oder via E-Mail
  an alle Mitglieder kommuniziert.
- Wird dem Aufruf zur Wiedergutmachung oder dem Clubraumverbot nicht
  nachgekommen, so sollte eine härtere Konsequenz folgen.
- Vereinsrechtliche Schritte oder strafrechtliche / zivilrechtliche
  Verfolgung bleibt von diesen Regelungen unberührt.
- Der Vorstand definiert sich durch die aktuell gültige Satzung.
- Keyholder sind in Teil 5 definiert.
