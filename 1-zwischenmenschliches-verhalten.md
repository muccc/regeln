## Teil 1: Zwischenmenschliches Verhalten

Im Umfeld des Clubs haben sich mehrere Leitsprüche eingebürgert, deren Ziel es
ist, das Zusammenleben zu gestalten.  Damit diese abstrakten Werte besser
verständlich werden, liefern wir hier Interpretationshilfen:

* "Be excellent to each other", das bedeutet für uns z.B.

  - Überdenke die Folgen deines Handelns und deiner Sprache. Witterst du ein
    Konfliktpotential, versuche dieses aus der Welt zu schaffen, bevor es
    eskaliert. Sprich mit den Leuten, um deine Intentionen für alle transparent
    zu machen.

  - Der private Bereich anderer ist ein Tabu, solange du nicht eingeladen
    wurdest, an diesem teilzunehmen. In Bezug auf Berührungen gilt nur ja heißt
    ja. Nie unaufgefordert. Körperliche und verbale Eingriffe in die
    Privatsphäre sind alles andere als "excellent". Bitte beachte auch, dass
    die Privatsphäre anderer einen größeren Radius haben kann als deine, da es
    jedem selbst zusteht, seine Privatsphäre zu definieren. Das heisst im
    Umkehrschluss aber auch, dass du, falls jemand deine Grenzen überschreitet,
    darauf hinweisen musst, dass hier deine Grenzen sind. Sei also auch offen
    für andere, die dir ihre Grenzen kommunizieren.

  - Da jeder den Club in einem nutzbaren Zustand vorfinden möchte, ist es
    angebracht, benutzte Räume wieder so zu hinterlassen, dass jemand anderes
    wieder "Spaß am Gerät" haben kann. Also die Küche nutzen kann, das Werkzeug
    an seinem Platz ist u.s.w. Kommen und kistenweise Zeug im Labor abstellen
    und wochenlang stehen zu lassen, Werkzeug rauskramen und zurücklassen wo man
    es eben genutzt hatte, seinen Pizzakarton auf der Couch liegen lassen ist
    eben nicht "excellent" zu denen, die den Club nach dir nutzen wollen.
    (Näheres dazu ist auch in Teil 2 geregelt.)

  - "Excellent" zu jemandem zu sein heißt nicht zwingend, alle gleich zu
    behandeln. Gleichbehandlung kann für Minderheiten weiterhin diskriminierend
    sein. Allen Gruppen und Individuen die gleichen Chancen zu geben kann
    bedeuten, dass einigen besondere Privilegien eingeräumt werden. Sofern
    Ideen und Ziele nicht im Widerspruch mit den unseren stehen,
    unterstützen wir die Ausprägungen mit dem größten Förderbedarf.

  - So gerne wir auch sehen würden, dass sich alle Mitglieder an dieses
    Regelwerk halten: Eine selbsternannte "Regelpolizei" ist sicherlich nicht im
    Sinne dieses Credos. Ein "überwachen" der Regeln oder eine Beschwerde über
    Regelverstöße ohne entstandenen Schaden für einzelne oder den Verein ist
    sicherlich nicht excellent. Noch schlimmer ist eine vorsätzliche
    Interpretation der Regeln um einer anderen, nicht öffentlich kommunizierten
    Agenda zu folgen oder einzelnen Clubmitgliedern zu schaden.

* "All creatures are welcome", das bedeutet für uns z.B.

  - Was die Akzeptanz von Anderen angeht sind wir offen für neues, offen für
    anderes. "Welcome" heißt, dass wir jedem und jeder symbolisch die Tür
    aufhalten, zu uns zu kommen, sich mit uns auseinanderzusetzen und sich den
    Club anzusehen. "Welcome" heißt aber nicht, dass wir jeden - ungeachtet
    seiner Ideale und seines Verhaltens - dauerhaft aufnehmen oder auch nur
    tolerieren. Wer sich - nach einer Anfangsphase der Eingewöhnung - nicht mit
    unseren politischen Grundsätzen und Regeln für das Zusammenleben anfreunden
    kann, dem legen wir hier nahe, wieder zu gehen. "All creatures are welcome"
    heißt nicht, dass auch alle bleiben müssen.

  - "All creatures" bezieht sich unserer Meinung nach auf die Divergenz in den
    in der Hackerethik genannten "üblichen Kriteren": *Beurteile einen Hacker
    nach dem, was er tut, und nicht nach üblichen Kriterien wie Aussehen, Alter,
    Abstammung, Geschlecht oder gesellschaftlicher Stellung.* Das schließt explizit
    nicht mit ein: mangelnde Hygiene, unangebrachte soziale Umgangsformen,
    Belästigung und ähnliches.

  - Explizit NOT WELCOME sind hingegen Kreaturen mit menschenverachtender
    Gesinnung.

### Anmerkungen zu Teil 1:

* Es handelt sich *nur* um eine Interpretationshilfe. Auch die Beispiele sind
  nicht konkret auf alles und jeden direkt anwendbar. Es sollte jedoch ein
  Kompass für jeden sein, einzuordnen wie weit die eigene Interpretation vom
  gemeinsamen Konsens abweicht.

* Wenn in diesem Teil neue Leitsprüche aufgenommen werden, sollten sie auch
  gleich um neue Beispiele und Interpretationshilfen ergänzt werden.
