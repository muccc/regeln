## Teil 3: Plenum

Die folgenden Regelungen wurden jeweils auf Plenen verabschiedet und regeln
die Plenen selbst:

- Regelmäßige Plenen finden jeden dritten Montag des Monats statt. Falls
  zu viele Themen anstehen, sollte gleich im Anschluss ein Auffangplenum
  angekündigt werden. Nach Möglichkeit soll dieses am ersten Montag des
  darauffolgenden Monats stattfinden.
- Weitere Plenen können in akuten Fällen mit mindestens 72h Vorlauf
  einberufen werden.
- Plenumspunkte sollen immer mind. 72h vor Beginn des Plenums auf der
  Plenumswikiseite eingetragen sein.
- Abstimmungen und daraus folgende Beschlüsse sind gültig, wenn die Abstimmung 
  72h vor Beginn des Plenums auf der Plenumswikiseite eingetragen war.
- Abstimmungsvorschläge können während des Plenum bearbeitet werden sofern
  sich dadurch das Thema nicht ändert.
- Das Anlegen von Abstimmungsvorschlägen wird begrüßt, ist aber nicht
  zwingend erforderlich.

### Anmerkungen zu Teil 3:

- Dieser Teil befindet sich in einer sehr frühen Entwurfsfassung und sollte
  in den kommenden Monaten stetig erweitert werden.
