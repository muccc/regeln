## Teil 4 "Keyholder":

Keyholder sind
- alle Mitglieder des MuCCC, die eine bestätigte, dauerhafte Schließberechtigung
  für die Räume des Clubs besitzen.
- sowie Gastkeyholder, die eine temporäre Schließberechtigung haben.

### Anforderungen
Der Normalfall ist, dass bekannte Mitglieder einen Schlüssel auf Wunsch bekommen
sollen. Dieser Schlüssel kommt mit der Verpflichtung, sich an den Geist des Clubs,
wie er in den Regeln festgehalten ist, zu halten.

“Bekannt” in diesem Sinn ist, wer

- an drei Plenen teilgenommen hat und
- drei Monate Mitglied ist und
- von einem Keyholder bei einem Plenum vorgeschlagen wird

Der Antrag wird auf einem Plenum gestellt. Dort kann es eine Aussprache (insbesondere
mit Erinnerung an die Regeln) geben, aber keine Abstimmung.

Bei einem Verstoß gegen die Regeln, kann der Schlüssel nach dem in den Regeln bestimmten
Prozess zeitweilig oder dauerhaft entzogen werden, vergleiche Teil 3.

### Gastkeys

Alle dauerhaften Keyholder können einen ssh Gastkey für andere Personen beantragen:
Der bürgende Keyholder reicht den Antrag durch einen Pull-Request in
[muccc/keyholder](https://git.muc.ccc.de/muccc/keyholder) ein. Ein andere Person
muss diesen mergen. Ein Gastkey ist zeitlich auf bis zu 3 Monate beschränkt und kann
verlängert werden.


### Rechte und Pflichten
- Die (Gast-)Keyholder haben Hausrecht. Sie können Personen bei deutlich falschem
  Verhalten jederzeit der Clubraeume verweisen - inklusive virtueller.
- Beim Verlassen ist sicherzustellen, dass noch andere (Gast-)Keyholder anwesend
  sind. Ist das nicht der Fall, ist dafür zu sogen, dass alle Personen den Club verlassen.
  Dies darf und muss direkt so gesagt werden.
