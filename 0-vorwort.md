# Verhaltensregeln im CCC München

## Vorwort

Das Regelwerk über das Verhalten im CCC München wurde im Sommer 2018 von einer
AG in mehreren Sitzungen erarbeitet. Es gilt sowohl für die physischen als auch
für die virtuellen Clubräume (z.B Chat, Mailingliste).

Es ist nicht als endgültiges Werk zu sehen, sondern kann als "work in progress"
betrachtet werden. Unstimmigkeiten und Unzulänglichkeiten können durch
Plenumsentscheide zu Nachbesserungen und Erweiterungen führen.

Es gliedert sich in mehrere Teile. Jeder Teil wurde mit einer eigenen Einführung
versehen und schließt mit weiteren Anmerkungen ab.
